package com.faysal.statusandquotes.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faysal.statusandquotes.Adapter.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import com.faysal.statusandquotes.DataBase.MyDatabasClass;
import com.faysal.statusandquotes.MainActivity;
import com.faysal.statusandquotes.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class StatusActivityEnglish extends AppCompatActivity {
    RecyclerView rv;
    Toolbar mToolbar;
    MyDatabasClass mdc;
    List<String> data;
    TextView toolbartext;
    String[] nam;

    InterstitialAd interstitial;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        MobileAds.initialize (this, getString (R.string.admob_app_id));

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface localTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Tekton.otf");
        toolbartext=(TextView)mToolbar.findViewById(R.id.toolbarText);
        toolbartext.setTypeface(localTypeface);




        rv=(RecyclerView)findViewById(R.id.my_recyclerView);
        nam = getResources().getStringArray(R.array.quotes_category);
        mdc=new MyDatabasClass(getApplicationContext());
        data=new ArrayList<>();


        int pos=getIntent().getIntExtra("keys",999);
        setDataClass(pos);
        setDataInAdpatr(data);
        toolBarSetText((pos-1));

        initBannerAds();
        inittIntersititalAds();


    }

    private void inittIntersititalAds(){
        AdRequest adIRequest = new AdRequest.Builder().build();

        // Prepare the Interstitial Ad Activity
        interstitial = new InterstitialAd(StatusActivityEnglish.this);

        // Insert the Ad Unit ID
        interstitial.setAdUnitId(getString(R.string.interstitial_ad_unit_id));

        // Interstitial Ad load Request
        interstitial.loadAd(adIRequest);

        // Prepare an Interstitial Ad Listener
        interstitial.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                // Call displayInterstitial() function when the Ad loads
                displayInterstitial();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Toast.makeText(StatusActivityEnglish.this, "Failed to load ads "+errorCode, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void displayInterstitial()
    {
        // If Interstitial Ads are loaded then show else show nothing.
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    private void initBannerAds() {
        final AdView mAdView = (AdView) findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Toast.makeText(StatusActivityEnglish.this, "Failed to load banner ads", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setDataClass(int pos){
        if (pos==1) {
            mdc.MyTableClass("sweet");
            data=mdc.getAllClass();
            setDataInAdpatr(data);

        }else if (pos==2){
            mdc.MyTableClass("life");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==3){
            mdc.MyTableClass("nice");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==4){
            mdc.MyTableClass("love");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==5){
            mdc.MyTableClass("emotional");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==6){
            mdc.MyTableClass("likeif");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==7){
            mdc.MyTableClass("breakup");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==8){
            mdc.MyTableClass("friendship");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==9){
            mdc.MyTableClass("missing");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==10){
            mdc.MyTableClass("birthday");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==11){
            mdc.MyTableClass("amazing");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==12){
            mdc.MyTableClass("sad");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==13){
            mdc.MyTableClass("school");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==14){
            mdc.MyTableClass("relationship");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==15){
            mdc.MyTableClass("selfish");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==16){
            mdc.MyTableClass("food");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==17){
            mdc.MyTableClass("clever");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==18){
            mdc.MyTableClass("hello");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==19){
            mdc.MyTableClass("crazy");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==20){
            mdc.MyTableClass("mans");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==21){
            mdc.MyTableClass("motivational");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==22){
            mdc.MyTableClass("kiss");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==23){
            mdc.MyTableClass("creative");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==24){
            mdc.MyTableClass("daring");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==25){
            mdc.MyTableClass("exam");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==26){
            mdc.MyTableClass("flirt");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==27){
            mdc.MyTableClass("goodluck");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==28){
            mdc.MyTableClass("crush");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==29){
            mdc.MyTableClass("morning");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==30){
            mdc.MyTableClass("rude");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==31){
            mdc.MyTableClass("attitude");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==32){
            mdc.MyTableClass("goodnight");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==33){
            mdc.MyTableClass("engagement");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==34){
            mdc.MyTableClass("anniversary");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==35){
            mdc.MyTableClass("cute");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==36){
            mdc.MyTableClass("wish");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==37){
            mdc.MyTableClass("womans");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==38){
            mdc.MyTableClass("broken");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==39){
            mdc.MyTableClass("decent");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }else if (pos==40){
            mdc.MyTableClass("love");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }
    }
    private void setDataInAdpatr(List<String> data){
        RecyclerAdapter adapter=new RecyclerAdapter(StatusActivityEnglish.this,data,false);
        rv.setAdapter(adapter);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        int id = item.getItemId();
        if (id== R.id.tranclatebtn){
                Intent intent = new Intent(getApplicationContext(), FavActivity.class);
                startActivity(intent);
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                StatusActivityEnglish.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void toolBarSetText(int pos){
        toolbartext.setText(nam[pos]);
    }





}
