package com.faysal.statusandquotes.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faysal.statusandquotes.Adapter.RecyclerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.List;

import com.faysal.statusandquotes.DataBase.FavDatabase;
import com.faysal.statusandquotes.R;

public class FavActivity extends AppCompatActivity {

    ListView listView;
    Toolbar mToolbar;
    TextView toolbartext;
    FavDatabase favDatabase;
    List<String> data;
    RecyclerView rv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        rv=(RecyclerView)findViewById(R.id.my_recyclerView);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface localTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Tekton.otf");
        toolbartext=(TextView)mToolbar.findViewById(R.id.toolbarText);
        toolbartext.setTypeface(localTypeface);
        toolbartext.setText("Bookmarks");
        favDatabase=new FavDatabase(getApplicationContext());
        data=favDatabase.getAllClass();


        if (favDatabase.returnCursor().getCount()==0){
            Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/sang.ttf");
            TextView t1 = (TextView)findViewById(R.id.t1);
            TextView t2 = (TextView)findViewById(R.id.t2);
            ImageView imageView=(ImageView)findViewById(R.id.imageView);
            LinearLayout layout=(LinearLayout)findViewById(R.id.visivilityGone);
            t1.setTypeface(typeface);
            t2.setTypeface(typeface);
            t1.setVisibility(View.VISIBLE);
            t2.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            layout.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }


        RecyclerAdapter adapter=new RecyclerAdapter(FavActivity.this,data,true);
        rv.setAdapter(adapter);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));

    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.tranclatebtn) {
            Intent intent = new Intent(getApplicationContext(), FavActivity.class);
            startActivity(intent);
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                FavActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
