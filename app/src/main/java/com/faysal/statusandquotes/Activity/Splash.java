package com.faysal.statusandquotes.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.faysal.statusandquotes.MainActivity;
import com.faysal.statusandquotes.R;

import static java.lang.Thread.sleep;

public class Splash extends AppCompatActivity {

    TextView t3;
    TextView t5;
    TextView t4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/pts.ttf");
        t3 = (TextView)findViewById(R.id.t3);
        t4 = (TextView)findViewById(R.id.t4);
        t5 = (TextView)findViewById(R.id.t5);
        t4.setText("Developed by Foysal Ahmed\nCopyright (c) BackBenchers Inc");
        t3.setTypeface(typeface);
        t4.setTypeface(typeface);
        t5.setTypeface(typeface);


        ImageView image = (ImageView) findViewById(R.id.image_splash);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.together);
        image.startAnimation(animation);

        new Thread()
        {
            public void run()
            {
                try
                {
                    sleep(1000L);
                    Intent localIntent = new Intent(Splash.this.getApplicationContext(), MainActivity.class);
                    Splash.this.startActivity(localIntent);
                    Splash.this.finish();
                    return;
                }
                catch (InterruptedException localInterruptedException)
                {
                    localInterruptedException.printStackTrace();
                }
            }
        }.start();
    }
}

