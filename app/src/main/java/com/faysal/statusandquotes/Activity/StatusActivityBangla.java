package com.faysal.statusandquotes.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faysal.statusandquotes.Adapter.RecyclerAdapter;
import com.faysal.statusandquotes.DataBase.MyDatabasClass;
import com.faysal.statusandquotes.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

public class StatusActivityBangla extends AppCompatActivity {
    RecyclerView rv;
    Toolbar mToolbar;
    MyDatabasClass mdc;
    List<String> data;
    TextView toolbartext;
    String[] nam;


    InterstitialAd interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface localTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Tekton.otf");
        toolbartext=(TextView)mToolbar.findViewById(R.id.toolbarText);
        toolbartext.setTypeface(localTypeface);


        rv=(RecyclerView)findViewById(R.id.my_recyclerView);
        nam = getResources().getStringArray(R.array.quotes_category);
        mdc=new MyDatabasClass(getApplicationContext());
        data=new ArrayList<>();


        int pos=getIntent().getIntExtra("keys",999);
        setDataClass(pos);
        setDataInAdpatr(data);
        toolBarSetText((pos-1));


        initBannerAds();
        inittIntersititalAds();

    }


    private void inittIntersititalAds(){
        AdRequest adIRequest = new AdRequest.Builder().build();

        // Prepare the Interstitial Ad Activity
        interstitial = new InterstitialAd(StatusActivityBangla.this);

        // Insert the Ad Unit ID
        interstitial.setAdUnitId(getString(R.string.interstitial_ad_unit_id));

        // Interstitial Ad load Request
        interstitial.loadAd(adIRequest);

        // Prepare an Interstitial Ad Listener
        interstitial.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded()
            {
                // Call displayInterstitial() function when the Ad loads
                displayInterstitial();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Toast.makeText(StatusActivityBangla.this, "Failed to load ads "+errorCode, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void displayInterstitial()
    {
        // If Interstitial Ads are loaded then show else show nothing.
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    private void initBannerAds() {
        final AdView mAdView = (AdView) findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Toast.makeText(StatusActivityBangla.this, "Failed to load banner ads", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void setDataClass(int pos){
        if (pos==1) {
            mdc.MyTableClass("sad_bl");
            data=mdc.getAllClass();
            setDataInAdpatr(data);

        }else if (pos==2){
            mdc.MyTableClass("life_bl");
            data=mdc.getAllClass();
            setDataInAdpatr(data);
        }
    }
    private void setDataInAdpatr(List<String> data){
        RecyclerAdapter adapter=new RecyclerAdapter(StatusActivityBangla.this,data,false);
        rv.setAdapter(adapter);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        int id = item.getItemId();
        if (id== R.id.tranclatebtn){
                Intent intent = new Intent(getApplicationContext(), FavActivity.class);
                startActivity(intent);
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                StatusActivityBangla.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void toolBarSetText(int pos){
        toolbartext.setText(nam[pos]);
    }





}
