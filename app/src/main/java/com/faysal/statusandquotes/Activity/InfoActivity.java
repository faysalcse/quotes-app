package com.faysal.statusandquotes.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.faysal.statusandquotes.R;

import me.anwarshahriar.calligrapher.Calligrapher;


public class InfoActivity extends AppCompatActivity {

    TextView devName;
    TextView devMail;

    Button rateUs;
    Button shareBtn;
    Button updateBtn;
    Button moreApp;

    ImageView homeAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/sang.ttf", true);

        devName=(TextView)findViewById(R.id.devName);
        devMail=(TextView)findViewById(R.id.devMail);

        devName.setText("Faysal Ahmed Shakil");
        devMail.setText("fsfoysal15@gmail.com");

        rateUs=(Button) findViewById(R.id.rateBtn);
        shareBtn=(Button)findViewById(R.id.shareBtn);
        updateBtn=(Button)findViewById(R.id.updateBtn);
        moreApp=(Button)findViewById(R.id.moreAppBtn);
        homeAction=(ImageView) findViewById(R.id.homeAction);

        homeAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName()));
                startActivity(intent);
            }
        });


        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url="https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName();
                Intent localIntent = new Intent();
                localIntent.setAction("android.intent.action.SEND");
                localIntent.putExtra("android.intent.extra.TEXT", url);
                localIntent.setType("text/plain");
                startActivity(Intent.createChooser(localIntent, "Chose one"));

            }
        });


        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName()));
                startActivity(intent);
            }
        });


        moreApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/dev?id=5465981101368041024"));
                startActivity(intent);

            }
        });


    }
}
