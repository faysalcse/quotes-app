package com.faysal.statusandquotes.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Foysal Ahmed Shakil on 6/15/2017.
 */

public class FavDatabase extends SQLiteOpenHelper {
    private final static String DB_NAME="fav_status.db";
    private final static String TAB_NAME="fav_status";

    private static final String SQL_CREATE = "CREATE TABLE " + TAB_NAME + "(" + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "status" + " TEXT) ";

    public FavDatabase(Context context) {
        super(context, DB_NAME,null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean AddData(String status){
        SQLiteDatabase database=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put("status",status);
        long result= database.insert(TAB_NAME,null,values);
        database.close();
        if (result==-1){
            return false;
        }
        else {
            return true;
        }
    }

    public List<String> getAllClass()
    {
        List<String> dataJokes=new ArrayList<>();
        Cursor cursor = getReadableDatabase().rawQuery("select * from " + TAB_NAME, null);
        String jokes;
        if (cursor.moveToFirst()) {
            do
            {
                jokes=cursor.getString(cursor.getColumnIndex("status"));
                dataJokes.add(new String(jokes));

            } while (cursor.moveToNext());
        }
        return dataJokes;
    }
        public int delete_bday(String bday){

        SQLiteDatabase s = this.getWritableDatabase();

        return s.delete(TAB_NAME, "status"+" = ?", new String[] {bday});

    }
    public Cursor returnCursor(){
        SQLiteDatabase database=this.getReadableDatabase();
        Cursor cursor = database.rawQuery("select * from " + TAB_NAME, null);
        return cursor;
    }

}
