package com.faysal.statusandquotes.DataBase;

import android.content.Context;
import android.database.Cursor;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Foysal Ahmed Shakil on 5/14/2017.
 */
public class MyDatabasClass extends SQLiteAssetHelper
{
    private static final String DATABASE_NAME = "my_status_db.db";
    private static final int DATABASE_VERSION = 1;
    public static String TABLE_NAME;
    private static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + "(" + "id" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "starttime" + " TEXT, " + "endtime" + " TEXT, " + "cname" + " TEXT, " + "tname" + " TEXT, " + "romnumber" + " TEXT, " + "subject" + " TEXT) ";

    public MyDatabasClass(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    public void MyTableClass(String tabname)
    {
        TABLE_NAME = tabname;
    }

    public List<String> getAllClass()
    {
        List<String> dataJokes=new ArrayList<>();
        Cursor cursor = getReadableDatabase().rawQuery("select * from " + TABLE_NAME, null);
        String jokes;
        if (cursor.moveToFirst()) {
            do
            {
                jokes=cursor.getString(cursor.getColumnIndex("st"));
                dataJokes.add(new String(jokes));

            } while (cursor.moveToNext());
        }
        return dataJokes;
    }
}
