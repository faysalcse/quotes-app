package com.faysal.statusandquotes;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

public class FullView extends AppCompatActivity {


    Button saveInStorage;
    FrameLayout layout;
    TextView status;
    Intent intent;
    String quotes;
    Button left,right,middle;
    Button choseImageForBG;
    public static final int GALLERY_REQUEST_CODE =2423;
    public static final int REQUEST_CODE =645;

    public static final String TAG=FullView.class.getSimpleName();
     ImageView resultImage;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_view);

        intent=getIntent();

        if (intent !=null){
            quotes=intent.getStringExtra("quotes");
        }

        saveInStorage=findViewById(R.id.saveInStorage);
        layout=findViewById(R.id.imageFrame);
        status=findViewById(R.id.status);

        choseImageForBG=findViewById(R.id.changeBackgroundImage);

        left=findViewById(R.id.leftText);
        middle=findViewById(R.id.middleText);
        right=findViewById(R.id.rightText);

        resultImage=findViewById(R.id.resultImage);


        if (quotes !=null){
            status.setText(quotes);
        }



        intiTextAlinmentOperations();
        initMainAction();




        saveInStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.v(TAG,"Permission is granted");
                        savePhotoOperations();
                    }else {
                        Toast.makeText(FullView.this, "Storage access permission not available.", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(FullView.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
                    }
                }

            }
        });

    }

    private void savePhotoOperations(){
       /* Bitmap layBitmap= viewToBitmap(layout);
        resultImage.setImageBitmap(layBitmap);

        String savedPath=saveToInternalStorage(layBitmap);

        if (savedPath !=null){
            Toast.makeText(FullView.this, "File saved in "+savedPath, Toast.LENGTH_SHORT).show();
            finish();
        }else {
            Toast.makeText(FullView.this, "Failed to save", Toast.LENGTH_SHORT).show();
        }*/

            Bitmap layBitmap= viewToBitmap(layout);
            if (layBitmap !=null){

                int num=new Random().nextInt(100);
                String filenName="quotes-"+num;

               String result = saveToInternalStorageFolder(layBitmap,filenName);
               if (result !=null){
                   Toast.makeText(this, "Image saved successfully Path : "+result, Toast.LENGTH_SHORT).show();
               }else {
                   Toast.makeText(this, "Failed to save image", Toast.LENGTH_SHORT).show();
               }
            }else {
                Toast.makeText(this, "Failed to parse bitmap", Toast.LENGTH_SHORT).show();
            }
    }

    private String createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/"+getString(R.string.app_name));

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/"+getString(R.string.app_name)+"/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File("/sdcard/"+getString(R.string.app_name)+"/", fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return String.valueOf(file);
    }

    private String saveToInternalStorageFolder(Bitmap bitmapImage,String s) {
        File directory = Environment.getExternalStorageDirectory();
        File childDirectory = new File(directory, getString(R.string.app_name));
        if (!childDirectory.exists()) {
            childDirectory.mkdirs();
        }

        // Create imageDir
        File mypath = new File(childDirectory, s+".jpeg");

        try {
            FileOutputStream fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return String.valueOf(mypath);
    }




    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }



    private void initMainAction(){

        choseImageForBG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int colour= ColorGenerator.MATERIAL.getRandomColor();
                layout.setBackgroundColor(colour);
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && requestCode==REQUEST_CODE){
            Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            savePhotoOperations();
        }else {
            Toast.makeText(this, "You need storage access permission to save photo ", Toast.LENGTH_SHORT).show();
        }
    }

    private void pickFromGallery(){
        //Create an Intent with action as ACTION_PICK
        Intent intent=new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
        // Launching the Intent
        startActivityForResult(intent,GALLERY_REQUEST_CODE);
    }

    private void intiTextAlinmentOperations(){

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status.setGravity(Gravity.LEFT);
            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status.setGravity(Gravity.RIGHT);
            }
        });

        middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status.setGravity(Gravity.CENTER);
            }
        });

    }



    public String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Quotes App Demo", Context.MODE_PRIVATE);
        // Create imageDir

        Random random=new Random();
        int number=random.nextInt(4);

        File mypath=new File(directory,"profile"+number+".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return directory.getAbsolutePath();
    }

    public Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }








}
