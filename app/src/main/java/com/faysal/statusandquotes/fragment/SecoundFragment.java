package com.faysal.statusandquotes.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.faysal.statusandquotes.Activity.StatusActivityBangla;
import com.faysal.statusandquotes.Adapter.CustomBaseAdapter;
import com.faysal.statusandquotes.R;

import java.util.ArrayList;
import java.util.List;

public class SecoundFragment extends Fragment {

    View view;
    GridView listView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.secound_tab,container,false);

        String[] _name = getResources().getStringArray(R.array.quotes_category_bl);
        String[] _total = getResources().getStringArray(R.array.total_bl);



        CustomBaseAdapter baseAdapter = new CustomBaseAdapter(_name, _total,getImageArray(), getActivity());
        listView = (GridView)view.findViewById(R.id.my_list_view);
        listView.setAdapter(baseAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), StatusActivityBangla.class);
                intent.putExtra("keys", (i + 1));
                startActivity(intent);

            }
        });

        return view;
    }

    List<Integer> getImageArray(){
        List<Integer> image=new ArrayList<>();
        image.add(R.drawable.image_1);
        image.add(R.drawable.sad_image);

        return image;
    }
}
