package com.faysal.statusandquotes.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.faysal.statusandquotes.fragment.FirstFragment;
import com.faysal.statusandquotes.fragment.SecoundFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new FirstFragment();
        }
        else if (position == 1)
        {
            fragment = new SecoundFragment();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "English";
        }
        else if (position == 1)
        {
            title = "Bangla";
        }
        return title;
    }
}