package com.faysal.statusandquotes.Adapter;

import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.Uri;
import android.widget.Toast;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.faysal.statusandquotes.Activity.FavActivity;
import com.faysal.statusandquotes.DataBase.FavDatabase;
import com.faysal.statusandquotes.FullView;
import com.faysal.statusandquotes.R;

import java.util.ArrayList;
import java.util.List;



import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by Foysal Ahmed Shakil on 5/23/2017.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerHolderNew> {

    List<String> status=new ArrayList<>();
    Context context;
    LayoutInflater inflater;
    boolean favchack;
    View view;

    public RecyclerAdapter(Context context, List<String> status,boolean favchack) {
        this.status = status;
        this.context = context;
        this.favchack=favchack;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public RecyclerHolderNew onCreateViewHolder(ViewGroup parent, int viewType) {
        view=inflater.inflate(R.layout.recycler_adapter_full,parent,false);
        RecyclerHolderNew holder=new RecyclerHolderNew(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerHolderNew holder, int position) {
       holder.status.setText(status.get(position));
       holder.statusno.setText("Status no : "+(position+1));
       Typeface typeface=Typeface.createFromAsset(context.getAssets(),"fonts/sang.ttf");
      /* holder.status.setTypeface(typeface);
       holder.statusno.setTypeface(typeface);*/

        holder.copy.setOnClickListener(this.copyListener);
        holder.copy.setTag(holder);
        holder.share.setOnClickListener(this.shareListener);
        holder.share.setTag(holder);
        holder.translate.setOnClickListener(this.translateListener);
        holder.translate.setTag(holder);
        if (favchack==true){
            holder.favstatus.setChecked(true);
        }else {
            holder.favstatus.setChecked(false);
        }
        holder.favstatus.setOnClickListener(this.favListener);
        holder.favstatus.setTag(holder);


    }

    public Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


    @Override
    public int getItemCount() {
        return status.size();
    }
    View.OnClickListener translateListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
             RecyclerHolderNew holder = (RecyclerHolderNew) paramAnonymousView.getTag();
            int i = holder.getPosition();
            try {

                Intent intent=new Intent(context, FullView.class);
                intent.putExtra("quotes",status.get(i));
                context.startActivity(intent);



            }catch (Exception e){

                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setIcon(R.drawable.ic_translates);
                builder.setTitle("Google Translate");
                builder.setMessage("Please install the Google Translate");
                builder.setNeutralButton("Install", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.translate"));
                        context.startActivity(intent);
                    }
                });

                builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                final AlertDialog dialog=builder.create();
                dialog.show();
            }
        }
    };
    View.OnClickListener copyListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            RecyclerHolderNew holder = (RecyclerHolderNew) paramAnonymousView.getTag();
            int i = holder.getPosition();
            ClipboardManager clipboardManager=(ClipboardManager)context.getSystemService(CLIPBOARD_SERVICE);
            clipboardManager.setText(status.get(i));
            Toast.makeText(context,"Successfully copied", Toast.LENGTH_SHORT).show();

        }
    };
    View.OnClickListener shareListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            RecyclerHolderNew holder = (RecyclerHolderNew) paramAnonymousView.getTag();
            int i = holder.getPosition();
            Intent localIntent = new Intent();
            localIntent.setAction("android.intent.action.SEND");
            localIntent.putExtra("android.intent.extra.TEXT",status.get(i));
            localIntent.setType("text/plain");
            context.startActivity(Intent.createChooser(localIntent, "Chose one"));

        }
    };
    View.OnClickListener favListener = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {

            RecyclerHolderNew holder = (RecyclerHolderNew) paramAnonymousView.getTag();
            FavDatabase database=new FavDatabase(context);
            int i = holder.getPosition();
            if (favchack==true) {
                if (database.returnCursor().getCount()==1){
                    Toast.makeText(context,"This status is Remove to the bookmark", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(context,FavActivity.class);
                    context.startActivity(intent);
                    database.delete_bday(status.get(i));
                    status.remove(status.get(i));
                    notifyDataSetChanged();
                }else {
                    Toast.makeText(context,"This status is Remove to the bookmark", Toast.LENGTH_SHORT).show();
                    database.delete_bday(status.get(i));
                    status.remove(status.get(i));
                    notifyDataSetChanged();
                }

            }else {
                if (holder.favstatus.isChecked()) {
                    database.AddData(status.get(i));
                    Toast.makeText(context,"This status is added to the bookmark", Toast.LENGTH_SHORT).show();

                }
            }
        }
    };
}
