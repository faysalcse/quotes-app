package com.faysal.statusandquotes.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.faysal.statusandquotes.R;

import java.util.List;

/**
 * Created by Foysal Ahmed Shakil on 5/13/2017.
 */
public class CustomBaseAdapter extends BaseAdapter {
    String[] title;
    String[] total;
    List<Integer> image;
    Context context;
    private static LayoutInflater inflater=null;

    public CustomBaseAdapter(String[] title, String[] total, List<Integer> image, Context context) {
        this.title = title;
        this.total = total;
        this.image = image;
        this.context = context;

        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CustomBaseAdapter(String[] title, String[] total, Context context) {
        this.title = title;
        this.context = context;
        this.total=total;


        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int i) {
        return title.length;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class MyHolder{
        TextView tv,total;
        ImageView iv;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        int colour= ColorGenerator.MATERIAL.getRandomColor();
        TextDrawable drawable = TextDrawable.builder().buildRoundRect(title[i].substring(0,1),colour,10);

        MyHolder holder=new MyHolder();
        View myview=inflater.inflate(R.layout.custom_adapter_grid,null);
        holder.tv=(TextView)myview.findViewById(R.id.title_text);
        holder.total=(TextView)myview.findViewById(R.id.total);
        holder.iv=(ImageView) myview.findViewById(R.id.item_image_id);

        if (image.size() !=0){
            holder.iv.setImageResource(image.get(i));
        }else {
            holder.iv.setImageDrawable(drawable);
        }

        holder.tv.setText(title[i]);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/pts.ttf");
            holder.tv.setTypeface(typeface);
            holder.total.setTypeface(typeface);
            holder.total.setText("Total Status : "+total[i]);


        return myview;
    }
}
