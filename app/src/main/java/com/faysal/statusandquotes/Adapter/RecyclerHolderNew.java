package com.faysal.statusandquotes.Adapter;

import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.faysal.statusandquotes.R;


public class RecyclerHolderNew extends RecyclerView.ViewHolder
{
    ImageButton copy;
    ImageButton translate;
    CheckBox favstatus;
    ImageButton share;
    TextView status;
    TextView statusno;
    FrameLayout imageFrame;

    public RecyclerHolderNew(View view)
    {
        super(view);
        status = (TextView)view.findViewById(R.id.status);
        statusno = (TextView)view.findViewById(R.id.statuseno);
        share = (ImageButton) view.findViewById(R.id.sahre_status);
        translate = (ImageButton) view.findViewById(R.id.tranclatebtn);
        copy = (ImageButton) view.findViewById(R.id.copy_status);
        favstatus = (CheckBox)view.findViewById(R.id.favoritestatus);
        imageFrame = (FrameLayout) view.findViewById(R.id.imageFrame);
    }
}
